const Task = require("../models/task");

module.exports.getAllTasks = () =>{
	return Task.find({}).then(result => result);
}

module.exports.createTask = (reqBody) =>{
	let newTask = new Task({
		name:reqBody.name,
	})

	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error)
			return false
		}
		else{
			return task
		}
	})
}


module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, reqBody) =>{
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.name = reqBody.name;

			return result.save().then((updatedTaskName, updateErr) =>{
				if(updateErr){
					console.log(updateErr);
					return false;
				} else{
					return updatedTaskName
				}
			})
		}


	})
}


module.exports.getspecificTask = (taskId) =>{
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		}
	})
}

module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return err;
		}
		result.status = "complete";
		return result.save().then((updatedTask, saveErr) => {	
			if (saveErr) {
				console.log(saveErr);
				return saveErr;
			} else {
				return updatedTask;
			}
		})
	})

}


